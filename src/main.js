var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var frame = 0
var state = 0
var rotation = 0
var resolution = 101

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < 8; i++) {
    for (var j = 0; j < resolution; j++) {
      fill(255 * ((i + 1) % 2))
      noStroke()
      push()
      if (state % 4 === 0) {
        translate(windowWidth * 0.5 + 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75, windowHeight * 0.5 + 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75)
      }
      if (state % 4 === 1) {
        translate(windowWidth * 0.5 - 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75, windowHeight * 0.5 + 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75)
      }
      if (state % 4 === 2) {
        translate(windowWidth * 0.5 - 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75, windowHeight * 0.5 - 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75)
      }
      if (state % 4 === 3) {
        translate(windowWidth * 0.5 + 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75, windowHeight * 0.5 - 0.1 * sin(frame * Math.PI) * (j - Math.floor(resolution * 0.5)) * boardSize * (1 / resolution) * 2.75)
      }
      rotate(rotation * Math.PI * ((i + 1) % 10) * 0.25)
      rect(0, 0, boardSize * (1 / pow(2, (i + 1))), boardSize * (1 / pow(2, (i + 1))))
      pop()
    }
  }

  frame += deltaTime * 0.0005
  rotation += deltaTime * 0.0005
  if (frame > 2) {
    frame = 0
    state++
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
